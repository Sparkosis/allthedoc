@extends('layouts.base')
@section('content')
<header>
@if(!Auth::guest())
<div class="pull-right">
               <a class="btn btn-default btn-sm" data-toggle="tooltip" title="Editer la page" href="/admin/page/{{$page['id']}}"><i class="fa fa-pencil"></i></a>
 </div>
 @endif
            <h1>{{$page['name']}}</h1>
            
            <p>{!! $page['description'] !!}</p>
            <ol class="toc">
            @foreach($page['section'] as $section)
              <li><a href="#{{$section['slug']}}">{{$section['titre']}}</a></li>
            @endforeach
            </ol>
          </header>
    @foreach($page['section'] as $section)
          <section>
            <h2 id="{{$section['slug']}}">{{$section['titre']}}</h2>
            {!! $section['content'] !!}
            

          </section>

   @endforeach
        
@endsection