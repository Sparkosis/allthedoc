@extends('layouts.base')
@section('content')
    <header>

        <h1>{{$hp['name']}}</h1>

        <p>{!! $hp['description'] !!}</p>
        <ol class="toc">
            @foreach($hp['section'] as $section)
                <li><a href="#{{$section['slug']}}">{{$section['titre']}}</a></li>
            @endforeach
        </ol>
    </header>
    @foreach($hp['section'] as $section)
        <section>
            <h2 id="{{$section['slug']}}">{{$section['titre']}}</h2>
            {!! $section['content'] !!}


        </section>

    @endforeach

@endsection