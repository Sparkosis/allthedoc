@foreach($links as $link)
  <li>
        <a href="/page/{{ $link['href'] }}">{{ $link['titre'] }}</a>
        @if (count($link['children']) > 0)
            <ul class="dropable">

    
                   @include('partials.Menu', array('links'=> $link['children']))
          
            </ul>
        @endif
    </li>
@endforeach