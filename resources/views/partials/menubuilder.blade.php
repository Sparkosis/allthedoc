@foreach($links as $link)

        <li class="dd-item" id="removeLi{{$link['id']}}" data-id="{{$link['id']}}">
        <div class="dd-handle">{{$link['titre']}}</div>

         
        @if (count($link['children']) > 0)
            <ol class="dd-list">

    
                   @include('partials.menubuilder', array('links'=> $link['children']))
          
            </ol>
        @endif
    </li>
@endforeach 
            
           