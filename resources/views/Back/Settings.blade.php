@extends('Back.layouts.base')


@section('content_header')
    <h1>Options du site</h1>

@stop

@section('content')

    <div class="row">
        <div class="col-sm-4">
            <a href="/admin/generateTranslations" class="btn btn-primary">(re)générer les traductions</a>

        </div>
    </div>
    <div class="row">

        <div class="col-sm-12">

            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Liste des options</h3>

                    <form class="form-horizontal" method="POST" action="">
                        {{csrf_field()}}
                        <div class="box-body">
                            @forelse($settings as $setting)
                                <input type="hidden" value="{{$setting['id']}}" name="id[{{$setting['id']}}]">

                                <div class="form-group {{ $errors->has('value') ? 'has-error' : '' }}">
                                    <label for="inputPassword3" class="col-sm-2 control-label">{{$setting['key']}}</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" placeholder="Valeur" value="{{$setting['value']}}" name="value[{{$setting['id']}}]">
                                    </div>
                                    @if ($errors->has('value'))
                                        <span class="help-block">
                            <strong>{{ $errors->first('value') }}</strong>
                        </span>
                                    @endif
                                </div>
                            @empty
                                <div class="alert alert-info">Aucune options</div>
                            @endforelse
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">

                            <button type="submit" class="btn btn-info pull-right">Modifier</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>

        </div>

    </div>

@stop
@section('js')



    <script src="/js/admin.js"></script>



@stop
@section('css')
@stop
