<?php
foreach ($links as $key => $link){

if($link['name'] == "Options" && !Auth::user()->can('edit settings')){
    unset($links[$key]);
}

    if($link['name'] == "Utilisateurs" && !Auth::user()->can('add users')){
        unset($links[$key]);
    }


}
?>
@foreach($links as $link)

    <li @if (count($link['children']) > 0) class="treeview" @endif>

        <a href="@if(empty($link['href'])) # @else {{ route($link['href']) }} @endif">
            <i class="fa fa-{{$link['icon']}}"></i>
            {{ $link['titre'] }}
            @if (count($link['children']) > 0)
            <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
            </span>
            @endif

        </a>
        @if (count($link['children']) > 0)
            <ul class="treeview-menu">


                @include('Back.partials.menu-item', array('links'=> $link['children']))
            </ul>
        @endif
    </li>
@endforeach

