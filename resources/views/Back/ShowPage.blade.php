@extends('Back.layouts.base')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Page n° {{$page['id']}}</h1>

@stop

@section('content')
    <div class="row">
        <div class="col-sm-4" style="margin-bottom: 5px;">
            <div class="box-tools">
                <a class="btn btn-default btn-sm" href="/{{$page['slug']}}"><i class="fa fa-eye"></i></a>
                <a class="btn btn-default btn-sm remove"><i class="fa fa-trash"></i></a>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-6">

            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Edition de la page</h3>

                    <form class="form-horizontal" method="POST" action="/admin/edit-page">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$page['id']}}">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Titre de la page</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name" placeholder="Titre" value="{{$page['name']}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Description</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" name="description">{{$page['description']}}</textarea>
                                </div>
                            </div>
                            <div id="section_field">

                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button id="add-section" class="btn btn-info">Ajouter une section</button>
                            <button type="submit" class="btn btn-info pull-right">Modifier</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>

        </div>
        <div class="col-sm-6">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Ordonner les sections</h3>
                </div>
                <div class="box-body">
                    <div class="dd">
                        <ol class="dd-list">
                            @foreach($page['section'] as $section)
                                <li class="dd-item" data-id="{{$section['id']}}">
                                    <div class="dd-handle">{{$section['titre']}}</div>
                                </li>
                            @endforeach

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($page['section'] as $section)
        <div class="row">
            <div class="col-sm-12">
                <!-- Default box -->
                <div class="box box-solid collapsed-box">
                    <div class="box-header">
                        <h3 class="box-title">{{$section['titre']}}</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-default btn-sm" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            <button class="btn btn-default btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div style="display: none;" class="box-body">
                        <form class="form-horizontal" method="POST" action="/admin/edit-section">
                            {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$section['id']}}">
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Titre de la section</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" placeholder="Titre" value="{{$section['titre']}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Contenu de la section</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="description">{{$section['content']}}</textarea>
                                    </div>
                                </div>
                                <div id="section_field">

                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">

                                <button type="submit" class="btn btn-info pull-right">Modifier</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    @endforeach

@stop
@section('js')


    <script src="/js/jquery.nestable.js"></script>

    <script>
        $(document).ready(function(){
            $('.dd').nestable({ maxDepth: 1});
            $('.dd').on('change', function (e) {
                $.post('/admin/EditPageOrder/', {
                    order: JSON.stringify($('.dd').nestable('serialize')),
                    id: {{$page['id']}},
                    _token: '{{ csrf_token() }}'
                }, function (data) {
                    toastr.success('{{ __('dashboard.success_update_menu') }}');
                });
            });

            $(".remove").on('click', function(e){
                e.preventDefault();
                var id_page = {{$page['id']}};
                swal({
                        title: "{{trans('dashboard.delete_sure')}}",

                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('dashboard.yes')}}",
                        cancelButtonText: "{{trans('dashboard.no')}}",
                        closeOnConfirm: true
                    },
                    function(){
                        $.post('/admin/DeletePage/', {
                            id: id_page,
                            _token: '{{ csrf_token() }}'
                        }, function (data) {

                            toastr.success("{{trans('dashboard.page_deleted')}}");
                        });

                    });
            });

        })
    </script>
@stop
@section('css')



@stop
