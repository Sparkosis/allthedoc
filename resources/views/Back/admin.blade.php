@extends('Back.layouts.base')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')

    <div class="row">

        <div class="col-sm-6">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Ajouter une page</h3>
                </div>
                <form class="form-horizontal" method="POST" action="">
                    {{csrf_field()}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Titre de la page</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" placeholder="Titre">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Description</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                        </div>
                        <div id="section_field">

                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button id="add-section" class="btn btn-info">Ajouter une section</button>
                        <button type="submit" class="btn btn-info pull-right">Envoyer</button>
                    </div>
                    <!-- /.box-footer -->
                </form>

            </div>
        </div>
        <div class="col-sm-6">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Liste des pages</h3>
                </div>
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @foreach(App\Pages::where('active',1)->get() as $page)
                            <li class="item" id="removeLi{{$page['id']}}">

                                <div class="product-info">
                                    <a href="/admin/page/{{$page['id']}}" class="product-title">{{$page['name']}}</a>
                                    <a data-id="{{$page['id']}}" class="bn btn-xs btn-danger pull-right remove" style="cursor: pointer"><i class="fa fa-trash"></i></a>
                                    <span class="product-description">
                         {{$page['description']}}
                        </span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </div>
    </div>
@stop
@section('js')

    <script type="text/javascript">
        $('.remove').on('click', function(e){
            e.preventDefault();
            var id_page = $(this).data('id');
            swal({
                    title: "{{trans('dashboard.delete_sure')}}",

                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('dashboard.yes')}}",
                    cancelButtonText: "{{trans('dashboard.no')}}",
                    closeOnConfirm: true
                },
                function(){
                    $.post('/admin/DeletePage/', {
                        id: id_page,
                        _token: '{{ csrf_token() }}'
                    }, function (data) {
                        var CurrentSelector = "#removeLi"+id_page;
                        $(CurrentSelector).remove();
                        toastr.success("{{trans('dashboard.page_deleted')}}");
                    });

                });

        });
    </script>
@stop