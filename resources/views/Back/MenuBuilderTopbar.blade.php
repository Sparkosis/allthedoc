@extends('Back.layouts.base')


@section('content_header')
    <h1>Menu Builder</h1>

@stop

@section('content')
    <div class="row">

        <div class="col-sm-4" style="margin-bottom: 5px;">

            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#Modal">Ajouter un lien</button>

        </div>

    </div>
    <div class="row">

        <div class="col-sm-6">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Menu TopBar</h3>

                </div>
                <div class="box-body">

                    <div class="dd">
                        <ol class="dd-list">
                            @include('partials.menubuilder', array('links'=> $menu))
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Liste des menu</h3>
                </div>
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @foreach(App\Menu::where('active', 1)->where('is_topbar', 1)->orderBy('order')->get() as $item)



                            <li class="item" id="removeLiDelete{{$item['id']}}">

                                <div class="product-info">
                                    <span class="product-title">{{$item['name']}}</span>
                                    <a data-id="{{$item['id']}}" class="bn btn-xs btn-danger pull-right remove" style="cursor: pointer"><i class="fa fa-trash"></i></a>
                                </div>
                            </li>

                        @endforeach
                    </ul>
                </div>

            </div>
        </div>
    </div>
    </div>
    <!-- Button trigger modal -->

    <!-- Modal -->
    <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form-horizontal" action="/admin/MenuBuilderAdd/" method="POST">
                    {{csrf_field()}}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Ajouter un élément au menu</h4>
                    </div>
                    <div class="modal-body">


                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Titre du menu</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="titre" placeholder="Titre">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Emplacement du menu</label>

                                <div class="col-sm-10">
                                    <select type="text" class="form-control" name="type">
                                        <option selected value="1">Topbar</option>
                                        <option value="0">Sidebar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Page de destination</label>

                                <div class="col-sm-10">
                                    <select name="lien" class="form-control" id="">
                                        @foreach($pages as $page)
                                            <option value="{{$page['slug']}}">{{$page['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div id="section_field">

                            </div>
                        </div>
                        <!-- /.box-body -->

                        <!-- /.box-footer -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-primary">Ajouter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')


    <script src="/js/admin.js"></script>
    <script src="/js/jquery.nestable.js"></script>

    <script>
        $(document).ready(function(){
            $('.dd').nestable({ maxDepth: 5});

            $('.dd').on('change', function (e) {
                $.post('/admin/MenuBuilderUpdate/', {
                    order: JSON.stringify($('.dd').nestable('serialize')),
                    _token: '{{ csrf_token() }}'
                }, function (data) {
                    toastr.success('{{ __('dashboard.success_update_menu') }}');
                });
            });


            $('.remove').on('click', function(e){
                e.preventDefault();
                var id_page = $(this).data('id');
                swal({
                        title: "{{trans('dashboard.delete_sure')}}",

                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('dashboard.yes')}}",
                        cancelButtonText: "{{trans('dashboard.no')}}",
                        closeOnConfirm: true
                    },
                    function(){
                        $.post('/admin/DeleteMenu/', {
                            id: id_page,
                            _token: '{{ csrf_token() }}'
                        }, function (data) {
                            var CurrentSelector = "#removeLi"+id_page;
                            var CurrentSelectorDelete = "#removeLiDelete"+id_page;
                            $(CurrentSelector).remove();
                            $(CurrentSelectorDelete).remove();
                            toastr.success("{{trans('dashboard.page_deleted')}}");
                        });

                    });

            });
        })
    </script>
@stop
@section('css')



@stop
