<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> @settings('Site_title') </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('adminlte/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/skin-blue.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @stack('css')
    @yield('css')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="/adminlte/index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>MD</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>All My DOCS </b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->
                    <li>
                        <a href="/" target="_blank"><i class="fa fa-eye"></i> Voir le site</a>
                    </li>
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="https://www.gravatar.com/avatar/{{md5(Auth::user()->email)}}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="https://www.gravatar.com/avatar/{{md5(Auth::user()->email)}}" class="img-circle" alt="User Image">

                                <p>
                                    {{Auth::user()->name}}

                                </p>
                            </li>
                            <!-- Menu Body -->

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="#" class="btn btn-default btn-flat">{{__('adminlte.log_out')}}</a>
                                </div>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="https://www.gravatar.com/avatar/{{md5(Auth::user()->email)}}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{Auth::user()->name}}</p>

                </div>
            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <?php
                $menu = new \App\Http\Controllers\MenuBuilderController();
                $menu = $menu->AdminMenu();
                ?>
                @include('Back.partials.menu-item', array('links'=> $menu))
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <section class="content-header">
            @yield('content_header')
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row" style="margin-bottom: 2em">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <button data-value="fr" class="btn btn-primary @if(Config::get('app.locale') == 'fr') active @endif setlocale">FR</button>
                        <button data-value="en" class="btn btn-primary @if(Config::get('app.locale') == 'en') active @endif setlocale">EN</button>

                    </div>
                </div>
            </div>

            @yield('content')

        </section>
    </div>
    <!-- /.content-wrapper -->



</div>
<!-- ./wrapper -->

<!-- jQuery 3.1.1 -->
<script src="{{ asset('adminlte/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('adminlte/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('adminlte/dist/js/app.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="/js/admin.js"></script>
<script>
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
@if(Session::has($msg))

toastr.{{ $msg }}('{{ Session::get($msg) }}');
    @endif
    @endforeach
</script>
<script src="/js/tinymce/tinymce.min.js"></script>
<script>tinymce.init({
        selector: 'textarea',
        language: 'fr_FR',
        plugins: 'codesample',
        toolbar: 'codesample',
        codesample_languages: [
            {text:'HTML/XML',value:'markup'},
            {text:"PHP",value:"php"},
            {text:"CSS",value:"css"},
            {text:"XML",value:"xml"},
            {text:"HTML",value:"html"},
            {text:"mathml",value:"mathml"},
            {text:"SVG",value:"svg"},
            {text:"Clike",value:"clike"},
            {text:"Javascript",value:"javascript"},
            {text:"ActionScript",value:"actionscript"},
            {text:"apacheconf",value:"apacheconf"},
            {text:"apl",value:"apl"},
            {text:"applescript",value:"applescript"},
            {text:"asciidoc",value:"asciidoc"},
            {text:"aspnet",value:"aspnet"},
            {text:"autoit",value:"autoit"},
            {text:"autohotkey",value:"autohotkey"},
            {text:"bash",value:"bash"},
            {text:"basic",value:"basic"},
            {text:"batch",value:"batch"},
            {text:"c",value:"c"},
            {text:"brainfuck",value:"brainfuck"},
            {text:"bro",value:"bro"},
            {text:"bison",value:"bison"},
            {text:"C#",value:"csharp"},
            {text:"C++",value:"cpp"},
            {text:"CoffeeScript",value:"coffeescript"},
            {text:"ruby",value:"ruby"},
            {text:"d",value:"d"},
            {text:"dart",value:"dart"},
            {text:"diff",value:"diff"},
            {text:"docker",value:"docker"},
            {text:"eiffel",value:"eiffel"},
            {text:"elixir",value:"elixir"},
            {text:"erlang",value:"erlang"},
            {text:"fsharp",value:"fsharp"},
            {text:"fortran",value:"fortran"},
            {text:"git",value:"git"},
            {text:"glsl",value:"glsl"},
            {text:"go",value:"go"},
            {text:"groovy",value:"groovy"},
            {text:"haml",value:"haml"},
            {text:"handlebars",value:"handlebars"},
            {text:"haskell",value:"haskell"},
            {text:"haxe",value:"haxe"},
            {text:"http",value:"http"},
            {text:"icon",value:"icon"},
            {text:"inform7",value:"inform7"},
            {text:"ini",value:"ini"},
            {text:"j",value:"j"},
            {text:"jade",value:"jade"},
            {text:"java",value:"java"},
            {text:"JSON",value:"json"},
            {text:"jsonp",value:"jsonp"},
            {text:"julia",value:"julia"},
            {text:"keyman",value:"keyman"},
            {text:"kotlin",value:"kotlin"},
            {text:"latex",value:"latex"},
            {text:"less",value:"less"},
            {text:"lolcode",value:"lolcode"},
            {text:"lua",value:"lua"},
            {text:"makefile",value:"makefile"},
            {text:"markdown",value:"markdown"},
            {text:"matlab",value:"matlab"},
            {text:"mel",value:"mel"},
            {text:"mizar",value:"mizar"},
            {text:"monkey",value:"monkey"},
            {text:"nasm",value:"nasm"},
            {text:"nginx",value:"nginx"},
            {text:"nim",value:"nim"},
            {text:"nix",value:"nix"},
            {text:"nsis",value:"nsis"},
            {text:"objectivec",value:"objectivec"},
            {text:"ocaml",value:"ocaml"},
            {text:"oz",value:"oz"},
            {text:"parigp",value:"parigp"},
            {text:"parser",value:"parser"},
            {text:"pascal",value:"pascal"},
            {text:"perl",value:"perl"},
            {text:"processing",value:"processing"},
            {text:"prolog",value:"prolog"},
            {text:"protobuf",value:"protobuf"},
            {text:"puppet",value:"puppet"},
            {text:"pure",value:"pure"},
            {text:"python",value:"python"},
            {text:"q",value:"q"},
            {text:"qore",value:"qore"},
            {text:"r",value:"r"},
            {text:"jsx",value:"jsx"},
            {text:"rest",value:"rest"},
            {text:"rip",value:"rip"},
            {text:"roboconf",value:"roboconf"},
            {text:"crystal",value:"crystal"},
            {text:"rust",value:"rust"},
            {text:"sas",value:"sas"},
            {text:"sass",value:"sass"},
            {text:"scss",value:"scss"},
            {text:"scala",value:"scala"},
            {text:"scheme",value:"scheme"},
            {text:"smalltalk",value:"smalltalk"},
            {text:"smarty",value:"smarty"},
            {text:"SQL",value:"sql"},
            {text:"stylus",value:"stylus"},
            {text:"swift",value:"swift"},
            {text:"tcl",value:"tcl"},
            {text:"textile",value:"textile"},
            {text:"twig",value:"twig"},
            {text:"TypeScript",value:"typescript"},
            {text:"verilog",value:"verilog"},
            {text:"vhdl",value:"vhdl"},
            {text:"wiki",value:"wiki"},
            {text:"YAML",value:"yaml"}
        ]
    });</script>
<script>
    $(document).ready(function(){
       $('.setlocale').click(function(){
           $.post('/admin/changelocale/', {
               lang: $(this).data('value'),
               _token: '{{ csrf_token() }}'
           }, function (data) {

               toastr.success("{{trans('dashboard.translation_changed')}}");
           });
       });
    });
</script>
@stack('js')
@yield('js')
</body>
</html>
