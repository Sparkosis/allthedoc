@extends('Back.layouts.base')


@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')


    <div class="row">
        <div class="col-sm-12">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Ajouter une page</h3>
                </div>
                <form class="form-horizontal" method="POST" action="">
                    {{csrf_field()}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Titre de la page</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" placeholder="Titre">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Description</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" name="description"></textarea>
                            </div>
                        </div>
                        <div id="section_field">

                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button id="add-section" class="btn btn-info">Ajouter une section</button>
                        <button type="submit" class="btn btn-info pull-right">Envoyer</button>
                    </div>
                    <!-- /.box-footer -->
                </form>

            </div>
        </div>
    </div>
@stop
@section('js')



@stop