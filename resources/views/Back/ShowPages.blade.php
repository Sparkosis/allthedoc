@extends('Back.layouts.base')


@section('content_header')
    <h1>Liste des pages</h1>

@stop

@section('content')


    <div class="row">

        <div class="col-sm-6">
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">Liste des pages</h3>
                </div>
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        @foreach(App\Pages::where('active',1)->get() as $page)
                            <li class="item" id="removeLi{{$page['id']}}">

                                <div class="product-info">
                                    <a href="/admin/page/{{$page['id']}}" class="product-title">{{$page['name']}}</a>
                                    <a data-id="{{$page['id']}}" class="bn btn-xs btn-danger pull-right remove" style="cursor: pointer"><i class="fa fa-trash"></i></a>
                                    <span class="product-description">
                         {{$page['description']}}
                        </span>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </div>
    </div>


@stop
@section('js')

    <script src="/js/admin.js"></script>
    <script src="/js/jquery.nestable.js"></script>

    <script>
        $(document).ready(function(){

            $(".remove").on('click', function(e){
                e.preventDefault();
                var id_page = $(this).data('id');
                swal({
                        title: "{{trans('dashboard.delete_sure')}}",

                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('dashboard.yes')}}",
                        cancelButtonText: "{{trans('dashboard.no')}}",
                        closeOnConfirm: true
                    },
                    function(){
                        $.post('/admin/DeletePage/', {
                            id: id_page,
                            _token: '{{ csrf_token() }}'
                        }, function (data) {

                            toastr.success("{{trans('dashboard.page_deleted')}}");
                        });

                    });
            });

        })
    </script>
@stop
@section('css')



@stop
