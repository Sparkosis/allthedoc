@extends('vendor.installer.layouts.master')

@section('title', "Etape finale")
@section('container')
    <p class="paragraph" style="text-align: center;">{{ session('message')['message'] }}</p>
    <div class="buttons">
        <a href="{{ url('/') }}" class="button">Aller sur la page d'accueil</a>
    </div>
@stop
