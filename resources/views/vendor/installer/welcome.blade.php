@extends('vendor.installer.layouts.master')

@section('title', "All the docs")
@section('container')
    <p class="paragraph" style="text-align: center;">Bienvenue sur All the docs</p>
    <div class="buttons">
        <a href="{{ route('LaravelInstaller::environment') }}" class="button">Etape suivante</a>
    </div>
@stop
