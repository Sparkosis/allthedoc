@extends('vendor.installer.layouts.master')

@section('title', "Prérequis")
@section('container')
    <ul class="list">
        @foreach($requirements['requirements'] as $extention => $enabled)
        <li class="list__item {{ $enabled ? 'success' : 'error' }}">{{ $extention }}</li>
        @endforeach
    </ul>

    @if ( ! isset($requirements['errors']))
        <div class="buttons">
            <a class="button" href="{{ route('LaravelInstaller::permissions') }}">
                Etape suivante
            </a>
        </div>
    @endif
@stop
