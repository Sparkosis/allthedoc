<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title>AllTheDocs</title>

    <!-- Styles -->
    <link href="/css/theDocs.all.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:100,300,400,500%7CLato:300,400' rel='stylesheet' type='text/css'>

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">
    <link rel="icon" href="/img/favicon.png">
  

  </head>

  <body>


    <!-- Sidebar -->
    <aside class="sidebar sidebar-boxed">

      <a class="sidebar-brand" href="/">AllTheDocs</a>

      <ul class="sidenav dropable">


   @include('partials.Menu', array('links'=> $menu))

      </ul>

    </aside>
    <!-- END Sidebar -->


    <header class="site-header navbar-fullwidth">

      <!-- Top navbar & branding -->
      <nav class="navbar navbar-default">
        <div class="container">

          <!-- Toggle buttons and brand -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="true" aria-controls="navbar">
              <span class="glyphicon glyphicon-option-vertical"></span>
            </button>

            <button type="button" class="navbar-toggle for-sidebar" data-toggle="offcanvas">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <!-- END Toggle buttons and brand -->

          <!-- Top navbar -->
          <div id="navbar" class="navbar-collapse collapse" aria-expanded="true" role="banner">
            <ul class="nav navbar-nav navbar-right">
           @foreach ($topbar as $nav)
          
              <li><a href="{{$nav['href']}}">{{$nav['titre']}}</a></li>

          @endforeach
            
            </ul>
          </div>
          <!-- END Top navbar -->

        </div>
      </nav>
      <!-- END Top navbar & branding -->
      
    </header>


    <main class="container-fluid">
      <div class="row">

        <!-- Main content -->
        <article class="main-content" role="main">
          
          @yield('content')
          
        </article>
        <!-- END Main content -->


        <!-- Footer -->


      </div>
    </main>

    <footer class="site-footer">
      <div class="container-fluid">
        <a id="scroll-up" href="#"><i class="fa fa-angle-up"></i></a>

        <div class="row">
          <div class="col-md-6 col-sm-6">
            <p>Copyright {{config("app.name")}} &copy; {{\Carbon\Carbon::now()->format('Y')}}. Tout droits réservés</p>
          </div>

        </div>
      </div>
    </footer>
    <!-- END Footer -->
    <!-- Scripts -->
    <script src="/js/theDocs.all.js"></script>
   <script type="text/javascript">
     $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $('pre').each(function(){
       $(this).addClass("line-numbers");
    });
});
   </script>


  </body>
</html>
