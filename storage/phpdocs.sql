-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `language_lines`;
CREATE TABLE `language_lines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `language_lines_group_index` (`group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `language_lines` (`id`, `group`, `key`, `text`, `created_at`, `updated_at`) VALUES
(1,	'dashboard',	'page_added',	'{\"en\":\"page added\",\"fr\":\"La page \\u00e0 bien \\u00e9t\\u00e9 ajout\\u00e9e\"}',	'2017-06-08 12:40:40',	'2017-06-08 12:40:40'),
(2,	'dashboard',	'page_edited',	'{\"en\":\"page modified\",\"fr\":\"La page \\u00e0 bien \\u00e9t\\u00e9 \\u00e9dit\\u00e9\"}',	'2017-06-08 12:40:40',	'2017-06-08 12:40:40'),
(3,	'dashboard',	'success_update_menu',	'{\"en\":\"the menu has been saved\",\"fr\":\"Le menu \\u00e0 bien \\u00e9t\\u00e9 sauvegard\\u00e9\"}',	'2017-06-08 12:40:40',	'2017-06-08 12:40:40'),
(4,	'dashboard',	'section_edited',	'{\"en\":\"the section :name has been saved\",\"fr\":\"La section :name \\u00e0 bien \\u00e9t\\u00e9 sauvegard\\u00e9\"}',	'2017-06-08 12:40:40',	'2017-06-08 12:40:40'),
(5,	'dashboard',	'success_add_menu',	'{\"en\":\"The menu has been added\",\"fr\":\"Le menu \\u00e0 bien \\u00e9t\\u00e9 ajout\\u00e9\"}',	'2017-06-08 12:40:40',	'2017-06-08 12:40:40'),
(6,	'dashboard',	'page_deleted',	'{\"en\":\"The menu has been deleted\",\"fr\":\"Page supprim\\u00e9e avec succ\\u00e9s\"}',	'2017-06-08 12:40:40',	'2017-06-08 12:40:40'),
(7,	'dashboard',	'delete_sure',	'{\"en\":\"Are you sure ?\",\"fr\":\"\\u00cates vous sur de bien vouloir supprim\\u00e9 cette page ?\"}',	'2017-06-08 12:40:40',	'2017-06-08 12:40:40'),
(8,	'dashboard',	'yes',	'{\"en\":\"yes\",\"fr\":\"oui\"}',	'2017-06-08 12:40:40',	'2017-06-08 12:40:40'),
(9,	'dashboard',	'no',	'{\"en\":\"no\",\"fr\":\"non\"}',	'2017-06-08 12:40:40',	'2017-06-08 12:40:40');

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT '0',
  `titre` varchar(30) NOT NULL,
  `is_topbar` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `href` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `menu` (`id`, `id_parent`, `titre`, `is_topbar`, `active`, `href`, `order`, `created_at`, `updated_at`) VALUES
(1,	NULL,	'Accueil',	0,	1,	'',	1,	NULL,	'2017-06-08'),
(2,	1,	'Sous Accueil',	0,	1,	'',	1,	NULL,	'2017-06-08'),
(3,	2,	'Sous Sous accueil',	0,	1,	'',	1,	NULL,	NULL),
(4,	3,	'Second menu',	0,	1,	'',	1,	NULL,	'2017-06-08'),
(5,	NULL,	'Accueil',	1,	1,	'/',	2,	NULL,	'2017-06-08'),
(6,	NULL,	'test_page',	0,	1,	'',	2,	NULL,	'2017-06-08'),
(7,	NULL,	'Ceci est un titre',	1,	1,	'/titre',	1,	'2017-06-08',	'2017-06-08'),
(8,	NULL,	'test',	1,	1,	'test',	3,	'2017-06-08',	'2017-06-08');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(3,	'2017_06_08_143317_create_language_lines_table',	2);

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `slug` varchar(30) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `pages` (`id`, `name`, `slug`, `active`, `created_at`, `updated_at`, `description`) VALUES
(1,	'test',	'test',	0,	'2017-06-07',	'2017-06-08',	'description'),
(2,	'Ttire',	'ttire',	0,	'2017-06-07',	'2017-06-08',	'<p>ceci est un titre</p>'),
(3,	'Ttire',	'ttire',	0,	'2017-06-07',	'2017-06-08',	'<p>ceci est un titre</p>'),
(4,	'Une page',	'une-page',	0,	'2017-06-07',	'2017-06-08',	'<p>Uqsnzqsqsd</p>'),
(5,	'tert',	'tert',	0,	'2017-06-07',	'2017-06-08',	'<p>ertertert</p>'),
(6,	'sdfsdf',	'sdfsdf',	0,	'2017-06-07',	'2017-06-08',	'<p>sdfsdf</p>'),
(11,	'Thet',	'thet',	1,	'2017-06-08',	'2017-06-08',	'<p>test</p>'),
(10,	'Thet',	'thet',	1,	'2017-06-08',	'2017-06-08',	'<p>test</p>'),
(9,	'sdf',	'sdf',	1,	'2017-06-07',	'2017-06-07',	'<p>ttestefsd</p>'),
(12,	'etzfsdf',	'etzfsdf',	1,	'2017-06-08',	'2017-06-08',	'<p>sdfsdsdfsdf</p>'),
(13,	'sdfsdf',	'sdfsdf',	1,	'2017-06-08',	'2017-06-08',	'<p>dsfsdf</p>'),
(14,	'sdfsdf',	'sdfsdf',	1,	'2017-06-08',	'2017-06-08',	'<p>dsfsdf</p>'),
(15,	'qsdqs',	'qsdqs',	1,	'2017-06-08',	'2017-06-08',	'<p>qsdqsd</p>');

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `sections`;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_page` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_page` (`id_page`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `sections` (`id`, `id_page`, `titre`, `slug`, `order`, `created_at`, `updated_at`, `content`) VALUES
(1,	1,	'1ere section',	'1ere-section',	1,	'2017-06-07',	'2017-06-08',	'<pre class=\"language-php\"><code>echo \"Op\"</code></pre>'),
(2,	1,	'2eme-section',	'2eme-section',	2,	'2017-06-07',	'2017-06-08',	''),
(3,	9,	'Test section',	'test-section',	3,	'2017-06-07',	'2017-06-07',	'<pre class=\"language-markup\"><code>qsdqsd</code></pre>');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'Nicolas ramos',	'contact@nicolas-r.fr',	'$2y$10$UpMmQEYphTwm1cpiCMaHdeHPK6DR1RT5MNGGfOJ2IUJMDB/w/XIse',	NULL,	'2017-06-07 07:49:52',	'2017-06-07 07:49:52');

-- 2017-06-09 12:14:09
