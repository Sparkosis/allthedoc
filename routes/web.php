<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'MainController@index')->name('home');

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::group(['prefix' => 'admin','middleware' => ['web', 'locale', 'auth']], function()
{

    Route::get('/', 'AdminController@index')->name('adminGet');
    Route::get('/refactor', 'AdminController@Refactor')->name('Refactor');
    Route::get('/menu-builder/sidebar', 'MenuBuilderController@Sidebar')->name('MenuBuilderAdmin');
    Route::get('/menu-builder/topbar', 'MenuBuilderController@Topbar')->name('menuBuilderTopBar');
    Route::get('/page-list', 'AdminController@pagelist')->name('pagesList');
    Route::get('/page/{id}', 'AdminController@ShowPage')->name('page');
    Route::get('/add-page', 'AdminController@add_page')->name('Addpage');
    Route::get('/settings', 'AdminController@SettingsShow')->name('Settings');
    Route::get('/generateTranslations', 'Translations@Create')->name('createTranslations');
    Route::get('/manage/users', 'ManageUserController@Liste')->name('manageuser');
    Route::post('/settings', 'AdminController@SettingsStore')->name('SettingsAdd');
    Route::post('/EditPageOrder/', 'AdminController@EditPageOrder')->name('pageOrder');
    Route::post('/edit-page/', 'AdminController@EditPage')->name('pageEdit');
    Route::post('/edit-section', 'AdminController@EditSection')->name('SectionEdit');
    Route::post('/MenuBuilderUpdate', 'MenuBuilderController@MenuBuilderUpdate')->name('MenuBuilderUpdate');
    Route::post('/MenuBuilderAdd', 'MenuBuilderController@MenuBuilderAdd')->name('MenuBuilderAdd');
    Route::post('/DeletePage', 'AdminController@DeletePage')->name('DeletePage');
    Route::post('/DeleteMenu', 'MenuBuilderController@DeleteMenu')->name('DeleteMenu');
    Route::post('/changelocale', ['as' => 'changelocale', 'uses' => 'AdminController@changeLocale']);



    Route::post('/', 'AdminController@store')->name('admin');
});


Route::get('/page/{pageslug}', 'MainController@page')->name('page');
	 




 Route::get('/install', 'InstallController@index')->name('indexInstall');
 Route::post('/install', 'InstallController@Step1')->name('indexInstall');
