<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdminMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adminMenu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_parent');
            $table->integer('active')->default(0);
            $table->integer('order')->nullable();
            $table->string('titre');
            $table->string('href');
            $table->string('icon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adminMenu');
    }
}
