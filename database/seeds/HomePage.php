<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class HomePage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'is_homepage' => 1,
        'slug' => "accueil",
        "name" => "Accueil",
        "description" => "Bienvenue sur la page d'accueil",
        "active" => 1
        ]);

        DB::table('menu')->insert([
        "id_parent" => 0,
        "active" => 1,
        "is_topbar" => 0,
        "titre" => "Accueil",
        "href" => "/accueil",
        "order" => 1
        ]);
        DB::table('adminMenu')->insert([
            "id_parent" => 0,
            "active" => 1,
            "titre" => "Administration",
            "href" => "adminGet",
            "icon" => "cogs",
            "order" => 1
        ]);

        DB::table('adminMenu')->insert([
            "id_parent" => 0,
            "active" => 1,
            "titre" => "Pages",
            "href" => "",
            "icon" => "files-o",
            "order" => 2
        ]);
        DB::table('adminMenu')->insert([
            "id_parent" => 2,
            "active" => 1,
            "titre" => "Ajouter une page",
            "href" => "Addpage",
            "icon" => "",
            "order" => 3
        ]);

        DB::table('adminMenu')->insert([
            "id_parent" => 2,
            "active" => 1,
            "titre" => "Liste des pages",
            "href" => "pagesList",
            "icon" => "",
            "order" => 4
        ]);
        DB::table('adminMenu')->insert([
            "id_parent" => 0,
            "active" => 1,
            "titre" => "Menu",
            "href" => "",
            "icon" => "bars",
            "order" => 5
        ]);

        DB::table('adminMenu')->insert([
            "id_parent" => 5,
            "active" => 1,
            "titre" => "Builder TopBar",
            "href" => "menuBuilderTopBar",
            "icon" => "bars",
            "order" => 6
        ]);
        DB::table('adminMenu')->insert([
            "id_parent" => 5,
            "active" => 1,
            "titre" => "Builder SideBar",
            "href" => "MenuBuilderAdmin",
            "icon" => "bars",
            "order" => 7
        ]);
    }
}
