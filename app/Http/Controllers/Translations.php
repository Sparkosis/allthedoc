<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\TranslationLoader\LanguageLine;


class Translations extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Create(Request $request){
        LanguageLine::truncate();
        LanguageLine::create([
            'group' => 'dashboard',
            'key' => 'page_added',
            'text' => ['en' => 'Page added ', 'fr' => 'La page à bien été ajoutée'],
        ]);
        LanguageLine::create([
            'group' => 'dashboard',
            'key' => 'page_edited',
            'text' => ['en' => 'Page edited', 'fr' => 'La page à bien été éditée'],
        ]);
        LanguageLine::create([
            'group' => 'dashboard',
            'key' => 'success_update_menu',
            'text' => ['en' => 'The menu has been saved', 'fr' => 'La menu à bien été mis à jour'],
        ]);
        LanguageLine::create([
            'group' => 'dashboard',
            'key' => 'section_edited',
            'text' => ['en' => ' The section :name has been saved', 'fr' => 'La section :name à bien été mis à jour'],
        ]);
        LanguageLine::create([
            'group' => 'dashboard',
            'key' => 'no',
            'text' => ['en' => 'No', 'fr' => 'Non'],
        ]);
        LanguageLine::create([
            'group' => 'dashboard',
            'key' => 'yes',
            'text' => ['en' => 'Yes', 'fr' => 'Oui'],
        ]);
        LanguageLine::create([
            'group' => 'dashboard',
            'key' => 'success_add_menu',
            'text' => ['en' => 'The menu has been added', 'fr' => 'Le menu à bien été ajouté'],
        ]);
        LanguageLine::create([
            'group' => 'dashboard',
            'key' => 'page_deleted',
            'text' => ['en' => 'The page has been deleted', 'fr' => 'La page à bien été supprimé'],
        ]);
        LanguageLine::create([
            'group' => 'dashboard',
            'key' => 'delete_sure',
            'text' => ['en' => 'Are you sure ?', 'fr' => 'Êtes vous sur ?'],
        ]);
        LanguageLine::create([
            'group' => 'dashboard',
            'key' => 'translation_generated',
            'text' => ['en' => 'Translation successfully generated', 'fr' => 'Traductions générés avec succes'],
        ]);
        LanguageLine::create([
            'group' => 'adminlte',
            'key' => 'log_out',
            'text' => ['en' => 'Logout', 'fr' => 'Déconnexion'],
        ]);
        LanguageLine::create([
            'group' => 'adminlte',
            'key' => 'remember_me',
            'text' => ['en' => 'Remember me', 'fr' => 'Se souvenir de moi'],
        ]);
        LanguageLine::create([
            'group' => 'adminlte',
            'key' => 'sign_in',
            'text' => ['en' => 'Login', 'fr' => 'Connexion'],
        ]);
        LanguageLine::create([
            'group' => 'adminlte',
            'key' => 'login_message',
            'text' => ['en' => 'Login Form', 'fr' => 'Formulaire de connexion'],
        ]);
        LanguageLine::create([
            'group' => 'adminlte',
            'key' => 'email',
            'text' => ['en' => 'Email', 'fr' => 'Email'],
        ]);
        LanguageLine::create([
            'group' => 'adminlte',
            'key' => 'password',
            'text' => ['en' => 'password', 'fr' => 'Mot de passe'],
        ]);
        LanguageLine::create([
            'group' => 'adminlte',
            'key' => 'password_reset_message',
            'text' => ['en' => 'Reset my password', 'fr' => 'Réinitialisé mon mot de passe'],
        ]);
        LanguageLine::create([
            'group' => 'adminlte',
            'key' => 'i_forgot_my_password ',
            'text' => ['en' => 'i forgot my password', 'fr' => 'Mot de passe oublié'],
        ]);
        LanguageLine::create([
            'group' => 'adminlte',
            'key' => 'send_password_reset_link',
            'text' => ['en' => 'Send password reset link', 'fr' => 'Envoyé mon nouveau mot de passe'],
        ]);
        LanguageLine::create([
            'group' => 'adminlte',
            'key' => 'retype_password',
            'text' => ['en' => 'Retype your password', 'fr' => 'Re tapez votre mot de passe'],
        ]);
        LanguageLine::create([
            'group' => 'adminlte',
            'key' => 'Modifier mon mot de passe',
            'text' => ['en' => 'Retype your password', 'fr' => 'Re tapez votre mot de passe'],
        ]);
        $request->session()->flash('success', trans('dashboard.translation_generated'));
        return redirect('/admin/settings');
    }
}
