<?php

namespace  App\Http\Controllers;


use App\adminMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Settings;
use App\Menu;
use App\Pages;
use App\Sections;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Back.admin');
    }


    /**
     * @return \Illuminate\Contracts\View\FaBack.ctory|\Illuminate\View\ViBack.ew
  Back.   */
    public function add_page(){
        return view('Back.addPage');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        $name = $request->name;
        $slug = str_slug($name, '-');
        $description = $request->description;
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);
        $id_page = Pages::create(["name" => $name,"slug" => $slug, "active" => 1, "description" => $description])->id;
        if($request->section_content){


            foreach( $request->section_content as $key=>$val){
                Sections::create(['titre' => $request->section_title[$key],
                    'content'  => $request->section_content[$key],
                    'id_page' => $id_page,
                    'slug' => str_slug($request->section_title[$key], '-')
                ]);
            }
        }
        $request->session()->flash('success', trans('dashboard.page_added'));
        return redirect()->route("adminGet");
    }
    public function changelocale(Request $request){
        $lang = $request->lang;

        $this->validate($request, ['lang' => 'required|in:fr,en']);

        \Session::put('locale', $lang);

        return redirect()->back();
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function EditPage(Request $request){

        $name = $request->name;
        $slug = str_slug($name, '-');
        $description = $request->description;
        $id_page = $request->id;
        $page = Pages::findOrFail($id_page);
        $page->name = $name;
        $page->slug = $slug;
        $page->description = $description;
        $page->save();
        if($request->section_content){


            foreach( $request->section_content as $key=>$val){
               Sections::create(['titre' => $request->section_title[$key],
                    'content'  => $request->section_content[$key],
                    'id_page' => $id_page,
                    'slug' => str_slug($request->section_title[$key], '-')
                ]);
            }
        }
        $request->session()->flash('success', trans('dashboard.page_edited'));
        return redirect('/admin/page/'.$id_page);
    }

    /**
     * @param Request $request
     */
    public function DeletePage(Request $request){
        $page = Pages::find($request->input('id'));
        $page->active = 0;
        $page->save();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function EditSection(Request $request){
        $name = $request->name;
        $slug = str_slug($name, '-');
        $description = $request->description;
        $id_section = $request->id;
        $page = Sections::findOrFail($id_section);
        $page->titre = $name;
        $page->slug = $slug;
        $page->content = $description;
        $page->save();
        $id_page = $page->id_page;
        $request->session()->flash('success', trans('dashboard.section_edited', ['name' => $name]));
        return redirect('/admin/page/'.$id_page);
    }

    /**
     * @return $this
     */
    public function pagelist(){
        $pages = Pages::where('active', 1)->get();
        $data['pages'] = $pages;
        return view('Back.ShowPages')->with($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function ShowPage(Request $request, $id){
        try{
            $pages = Pages::findOrFail($id);
            $data['page'] = $pages;
            return view('Back.ShowPage')->with($data);
        } catch(Exception $e){
            abort(404);
        }
    }

    /**
     * @param Request $request
     */
    public function EditPageOrder(Request $request){

        $menuItemOrder = json_decode($request->input('order'));
        $id_page = $request->id;

        foreach ($menuItemOrder as $index => $menuItem) {
            $item = Sections::findOrFail($menuItem->id);
            $item->order = $index + 1;

            $item->save();
        }
    }

    //Settings

    public function SettingsShow(Request $request){


        $data['settings'] = Settings::where('active', 1)->get();
        return view('Back.Settings')->with($data);
    }
    public function SettingsStore(Request $request){


        $this->validate($request, [
            'value.*' => 'required',
            'id.*' => 'required',
        ]);

        foreach($request->input('value') as $key=>$val){

            $id = $request->id[$key];
            $Setting = Settings::findOrFail($id);
            $Setting->value = $request->input('value');
            $Setting->save;

        }
        $request->session()->flash('success', trans('dashboard.page_added'));
        return redirect('/admin/settings');
    }

}
