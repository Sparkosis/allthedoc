<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\Pages;
class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
     
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {




    $menu = Menu::where('active', 1)->Where('is_topbar', 0)->get();
    $menu = $this->buildTree($menu);
    $data['menu'] = $menu;
    $data['topbar'] = Menu::where('active', 1)->Where('is_topbar', 1)->get();
    $id_hp = Pages::where('is_homepage', 1)->value('id');
    $data['hp'] = Pages::find($id_hp);
        return view('Front.index')->with($data);
    }

      private function buildTree($elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element->id_parent == $parentId) {
                $children = $this->buildTree($elements, $element->id);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$element->id] = $element;
                unset($elements[$element->id]);
            }
        }
        return $branch;
    }

    public function Page($page){
    $menu = Menu::where('active', 1)->Where('is_topbar', 0)->orderBy('order')->get();
    $menu = $this->buildTree($menu);
    $data['menu'] = $menu;
    $data['topbar'] = Menu::where('active', 1)->Where('is_topbar', 1)->orderBy('order')->get();

        $page = Pages::where('slug', $page)->where('active', 1);

        if(!$page->exists()){
            abort(404);
        } else {
            $page_id = $page->value('id');
            $page = Pages::find($page_id);
            $data['page'] = $page;
            return view('Front.page')->with($data);
        }

    }

}
