<?php

namespace App\Http\Controllers;

use App\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
class ManageUserController extends Controller
{
    public function Liste (){

        return redirect('/admin');
    }

    private function GenerateRolesAndPermissions(){
        Role::create(['name' => 'SuperAdmin']);
        Role::create(['name' => 'Admin']);
        Role::create(['name' => 'Redactor']);

        Permission::create(['name' => 'edit settings']);
        Permission::create(['name' => 'edit pages']);
        Permission::create(['name' => 'edit menu']);
        Permission::create(['name' => 'add menu']);
        Permission::create(['name' => 'add pages']);
        Permission::create(['name' => 'add users']);
        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'delete users']);
        Permission::create(['name' => 'delete pages']);
        Permission::create(['name' => 'delete menu']);


    }
    private function GeneratePermissionsToRoles(){
        $SuperAdmin = Role::find(1);
        $Admin = Role::find(2);
        $Redactor = Role::find(3);

        $SuperAdmin->givePermissionTo('edit settings');
        $SuperAdmin->givePermissionTo('edit pages');
        $SuperAdmin->givePermissionTo('edit menu');
        $SuperAdmin->givePermissionTo('add pages');
        $SuperAdmin->givePermissionTo('add menu');
        $SuperAdmin->givePermissionTo('add users');
        $SuperAdmin->givePermissionTo('delete users');
        $SuperAdmin->givePermissionTo('delete pages');
        $SuperAdmin->givePermissionTo('delete menu');

        $Admin->givePermissionTo('edit settings');
        $Admin->givePermissionTo('edit pages');
        $Admin->givePermissionTo('edit menu');
        $Admin->givePermissionTo('add pages');
        $Admin->givePermissionTo('add menu');
        $Admin->givePermissionTo('add users');
        $Admin->givePermissionTo('delete pages');
        $Admin->givePermissionTo('delete menu');


        $Redactor->givePermissionTo('edit pages');
        $Redactor->givePermissionTo('edit menu');
        $Redactor->givePermissionTo('add pages');
        $Redactor->givePermissionTo('add menu');
        $Redactor->givePermissionTo('delete pages');
        $Redactor->givePermissionTo('delete menu');
    }
}
