<?php

namespace App\Http\Controllers;

use App\adminMenu;
use App\Pages;
use Illuminate\Http\Request;
use App\Menu;
class MenuBuilderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Sidebar(){
    	$menu = $this->buildTree(Menu::where('active', 1)->where('is_topbar', 0)->orderBy('order')->get());
        $pages = Pages::where('active', 1)->get();
    	$data['menu'] = $menu;
        $data['pages'] = $pages;

    	return view('Back.MenuBuilderSidebar')->with($data);
    }
    public function DeleteMenu(Request $request){
        $page = Menu::find($request->input('id'));
        $page->active = 0;
        $page->save();
    }
     public function Topbar(){
         $pages = Pages::where('active', 1)->get();
    		$menu = $this->buildTree(Menu::where('active', 1)->where('is_topbar', 1)->orderBy('order')->get());
             $data['pages'] = $pages;
    		$data['menu'] = $menu;
    

    	return view('Back.MenuBuilderTopbar')->with($data);
    }
    public function MenuBuilderUpdate(Request $request){

        $menuItemOrder = json_decode($request->input('order'));
        $this->orderMenu($menuItemOrder, null);
    }
    private function orderMenu(array $menuItems, $parentId)
    {
        foreach ($menuItems as $index => $menuItem) {
            $item = Menu::findOrFail($menuItem->id);
            $item->order = $index + 1;
            $item->id_parent = $parentId;
            $item->save();
            if (isset($menuItem->children)) {
                $this->orderMenu($menuItem->children, $item->id);
            }
        }
    }
    public function AdminMenu(){
        $menu = $this->buildTree(adminMenu::where('active', 1)->orderBy('order')->get());
        return $menu;

    }
    public function MenuBuilderAdd(Request $request){

    	$type = $request->input('type');
    	$lien = $request->input('lien');
    	$titre = $request->input('titre');
    	Menu::create(["id_parent" => 0, "titre" => $titre, "is_topbar" => $type, "href" => $lien, "active" => 1]);
    	 $request->session()->flash('success', trans('dashboard.page_added'));
            return redirect()->route("MenuBuilderAdmin");
    }
    
    	private function buildTree($elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element->id_parent == $parentId) {
                $children = $this->buildTree($elements, $element->id);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$element->id] = $element;
                unset($elements[$element->id]);
            }
        }
        return $branch;
    }
    
}