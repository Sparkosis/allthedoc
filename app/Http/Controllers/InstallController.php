<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Spatie\TranslationLoader\LanguageLine;
class InstallController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    protected function changeEnv($data = array()){
        if(count($data) > 0){

            // Read .env-file
            $env = file_get_contents(base_path() . '/.env');

            // Split string on every " " and write into array
            $env = preg_split('/\s+/', $env);;

            // Loop through given data
            foreach((array)$data as $key => $value){

                // Loop through .env-data
                foreach($env as $env_key => $env_value){

                    // Turn the value into an array and stop after the first split
                    // So it's not possible to split e.g. the App-Key by accident
                    $entry = explode("=", $env_value, 2);

                    // Check, if new key fits the actual .env-key
                    if($entry[0] == $key){
                        // If yes, overwrite it with the new one
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        // If not, keep the old one
                        $env[$env_key] = $env_value;
                    }
                }
            }

            // Turn the array back to an String
            $env = implode("\n", $env);

            // And overwrite the .env with the new data
            file_put_contents(base_path() . '/.env', $env);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('install.index');
    }

    public function Step1(Request $request){


$servername = $request->servername;
$username = $request->username;
$password = $request->pass;
$dbname = $request->dbname;
$adminUsername = $request->adminUsername;
$adminPass = $request->adminPass;
        try {
            $conn = new \PDO("mysql:host=$servername", $username, $password);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $sql = "CREATE DATABASE IF NOT EXISTS $dbname";
            $conn->exec($sql);
            echo "DB created successfully";
            copy(base_path() . '/.env.example', base_path()."/.env");
            $env_update = $this->changeEnv([
                'DB_DATABASE'   => $dbname,
                'DB_USERNAME'   => $username,
                'DB_PASSWORD'   => $password,
                'DB_HOST'       => $servername
            ]);
            if($env_update){
             Artisan::call("key:generate");
             Artisan::call("migrate", ['--seed' => true]);
             User::create([
                 'name' => "admin",
                 'email' => $adminUsername,
                 'password' => bcrypt($adminPass),
             ]);
                LanguageLine::truncate();
                LanguageLine::create([
                    'group' => 'dashboard',
                    'key' => 'page_added',
                    'text' => ['en' => 'Page added ', 'fr' => 'La page à bien été ajoutée'],
                ]);
                LanguageLine::create([
                    'group' => 'dashboard',
                    'key' => 'page_edited',
                    'text' => ['en' => 'Page edited', 'fr' => 'La page à bien été éditée'],
                ]);
                LanguageLine::create([
                    'group' => 'dashboard',
                    'key' => 'success_update_menu',
                    'text' => ['en' => 'The menu has been saved', 'fr' => 'La menu à bien été mis à jour'],
                ]);
                LanguageLine::create([
                    'group' => 'dashboard',
                    'key' => 'section_edited',
                    'text' => ['en' => ' The section :name has been saved', 'fr' => 'La section :name à bien été mis à jour'],
                ]);
                LanguageLine::create([
                    'group' => 'dashboard',
                    'key' => 'no',
                    'text' => ['en' => 'No', 'fr' => 'Non'],
                ]);
                LanguageLine::create([
                    'group' => 'dashboard',
                    'key' => 'yes',
                    'text' => ['en' => 'Yes', 'fr' => 'Oui'],
                ]);
                LanguageLine::create([
                    'group' => 'dashboard',
                    'key' => 'success_add_menu',
                    'text' => ['en' => 'The menu has been added', 'fr' => 'Le menu à bien été ajouté'],
                ]);
                LanguageLine::create([
                    'group' => 'dashboard',
                    'key' => 'translation_changed',
                    'text' => ['en' => 'The language has been changed', 'fr' => 'La langue à été changée'],
                ]);
                LanguageLine::create([
                    'group' => 'dashboard',
                    'key' => 'page_deleted',
                    'text' => ['en' => 'The page has been deleted', 'fr' => 'La page à bien été supprimé'],
                ]);
                LanguageLine::create([
                    'group' => 'dashboard',
                    'key' => 'delete_sure',
                    'text' => ['en' => 'Are you sure ?', 'fr' => 'Êtes vous sur ?'],
                ]);
                LanguageLine::create([
                    'group' => 'dashboard',
                    'key' => 'translation_generated',
                    'text' => ['en' => 'Translation successfully generated', 'fr' => 'Traductions générés avec succes'],
                ]);
                LanguageLine::create([
                    'group' => 'adminlte',
                    'key' => 'log_out',
                    'text' => ['en' => 'Logout', 'fr' => 'Déconnexion'],
                ]);
                LanguageLine::create([
                    'group' => 'adminlte',
                    'key' => 'remember_me',
                    'text' => ['en' => 'Remember me', 'fr' => 'Se souvenir de moi'],
                ]);
                LanguageLine::create([
                    'group' => 'adminlte',
                    'key' => 'sign_in',
                    'text' => ['en' => 'Login', 'fr' => 'Connexion'],
                ]);
                LanguageLine::create([
                    'group' => 'adminlte',
                    'key' => 'i_forgot_my_password',
                    'text' => ['en' => 'I forgot my password', 'fr' => 'J\'ai oublié mont mot de passe'],
                ]);
                LanguageLine::create([
                    'group' => 'adminlte',
                    'key' => 'login_message',
                    'text' => ['en' => 'Login Form', 'fr' => 'Formulaire de connexion'],
                ]);
                LanguageLine::create([
                    'group' => 'adminlte',
                    'key' => 'email',
                    'text' => ['en' => 'Email', 'fr' => 'Email'],
                ]);
                LanguageLine::create([
                    'group' => 'adminlte',
                    'key' => 'password',
                    'text' => ['en' => 'password', 'fr' => 'Mot de passe'],
                ]);
                LanguageLine::create([
                    'group' => 'adminlte',
                    'key' => 'password_reset_message',
                    'text' => ['en' => 'Reset my password', 'fr' => 'Réinitialisé mon mot de passe'],
                ]);
                LanguageLine::create([
                    'group' => 'adminlte',
                    'key' => 'i_forgot_my_password ',
                    'text' => ['en' => 'i forgot my password', 'fr' => 'Mot de passe oublié'],
                ]);
                LanguageLine::create([
                    'group' => 'adminlte',
                    'key' => 'send_password_reset_link',
                    'text' => ['en' => 'Send password reset link', 'fr' => 'Envoyé mon nouveau mot de passe'],
                ]);
                LanguageLine::create([
                    'group' => 'adminlte',
                    'key' => 'retype_password',
                    'text' => ['en' => 'Retype your password', 'fr' => 'Re tapez votre mot de passe'],
                ]);

            } else {
                echo 'une erreur sest produite';
            }
            // more code
            Storage::put('installed', "");
        }
        catch(\PDOException $e)
        {
            echo $e->getMessage();
        }


    }
    

}
