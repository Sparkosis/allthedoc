<?php
namespace App;
use Illuminate\Database\Eloquent\Model as Model;

class Sections extends Model{
	protected $table = "sections";
		protected $guarded = [];
	 public function page()
    {
        return $this->belongsTo('App\Pages');
    }
	
}