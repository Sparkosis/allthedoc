<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $guarded = [];
    public function get($key = ""){
        return Settings::where('key', $key)->value('value');
    }
}
