<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class adminMenu extends Model
{
    protected $guarded = [];
    protected $table = "adminMenu";
}
