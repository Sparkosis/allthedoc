<?php
namespace App;
use Illuminate\Database\Eloquent\Model as Model;

class Pages extends Model{
	protected $table = "pages";
	protected $guarded = [];
	public function section(){
		return $this->hasMany('App\Sections', 'id_page', 'id')->orderBy('order');
	}
}